class Fixnum
  def in_words
    ones = { 0 => "zero", 1 => "one", 2 => "two", 3 => "three",
             4 => "four", 5 => "five", 6 => "six", 7 => "seven",
             8 => "eight", 9 => "nine"}

    tens = { 20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty",
            60 => "sixty",70 => "seventy",80 => "eighty",90 => "ninety" }

    teens = { 10 => "ten", 11 => "eleven", 12 => "twelve", 13 => "thirteen",
              14 => "fourteen", 15 => "fifteen", 16 => "sixteen",
              17 => "seventeen", 18 => "eighteen", 19 => "nineteen" }

    scale = { 100 => "hundred", 1000 => "thousand",
               1_000_000 => "million", 1_000_000_000 => "billion",
               1_000_000_000_000 => "trillion" }

    if (self < 10)
      ones[self]
    elsif (self < 20)
      teens[self]
    elsif (self < 100)
      tens_word = tens[(self / 10) * 10]
      if (self % 10) != 0
        tens_word + " " + (self % 10).in_words
      else
        tens_word
      end
    else
      magnitude = find_magnitude
      magnitude_words =
        (self / magnitude).in_words + " " + scale[magnitude]
      if (self % magnitude) != 0
        magnitude_words + " " + (self % magnitude).in_words
      else
        magnitude_words
      end
    end
  end

  def find_magnitude
    scale = { 100 => "hundred", 1000 => "thousand",
               1_000_000 => "million", 1_000_000_000 => "billion",
               1_000_000_000_000 => "trillion" }
    scale.keys.take_while { |magnitude| magnitude <= self }.last
  end
end




    # words_hash = { 0 => 'zero', 1 => "one", 2 => "two", 3 => "three", 4 => "four",
    #                5 => "five", 6 => "six", 7 => "seven", 8 => "eight",
    #                9 => "nine", 10 => "ten", 11 => "eleven", 12 => "twelve",
    #                13 => "thirteen", 14 => "fourteen", 15 => "fifteen",
    #                16 => "sixteen", 17 => "seventeen", 18 => "eighteen",
    #                19 => "nineteen", 20 => "twenty", 30 => "thirty",
    #                40 => "forty", 50 => "fifty", 60 => "sixty",
    #                70 => "seventy", 80 => "eighty", 90 => "ninety"}
    # scale_hash = { 100 => 'hundred', 1000 => 'thousand', 1_000_000 => 'million',
    #                1_000_000_000 => 'billion', 1_000_000_000_000 => 'trillion' }
    #
    # words_hash[self] if words_hash.key?(self)
    #
